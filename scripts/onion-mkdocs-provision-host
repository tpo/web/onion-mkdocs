#!/bin/bash
#
# Provisioner for the self-hosted environment.
#

# Basic dependencies
DEPENDENCIES="apache2"

# Ensure an up-to-date system
sudo apt-get update && sudo apt-get dist-upgrade -y && \
  sudo apt-get autoremove -y && sudo apt-get clean

# Install dependencies
sudo apt install -y $DEPENDENCIES

# Configure an Onion Service
trashman install tor tor-onion-service

# Configure virtual host for the Onion Service
cat <<-EOF | sudo tee /etc/apache2/sites-available/onion.conf > /dev/null
<VirtualHost *:80 *:8000>
    ServerName localhost
    ServerAlias *.onion
    DocumentRoot "/srv/shared/public"

    <Directory /srv/shared/public>
      AuthType Basic
      AuthName "Protected"
      AuthUserFile /srv/shared/.htpasswd
      Require valid-user
    </Directory>
</VirtualHost>
EOF

# Configure virtual host for the local service
cat <<-EOF | sudo tee /etc/apache2/sites-available/local.conf > /dev/null
<VirtualHost *:80 *:8000>
    ServerName onion-mkdocs.local
    DocumentRoot "/srv/shared/public"

    <Directory /srv/shared/public>
      Options Indexes FollowSymLinks
      AllowOverride All
      Require all granted
    </Directory>
</VirtualHost>
EOF

# Configure port 8000
if ! grep -q 'Listen 8000' /etc/apache2/ports.conf; then
  echo "Listen 8000" | sudo tee -a /etc/apache2/ports.conf
fi

# Enable virtual host
sudo a2ensite onion local
sudo systemctl reload apache2

# Configure PATH
mkdir -p ~/.custom
echo 'export PATH=$PATH:/srv/shared/scripts' > ~/.custom/profile
