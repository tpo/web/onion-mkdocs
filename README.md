# MkDocs Material template for Tor documentation

This is a [MkDocs][] documentation template for [Tor][]-related projects and
based on the [Material theme][].

Check the documentation and live version at
[https://tpo.pages.torproject.net/web/onion-mkdocs/](https://tpo.pages.torproject.net/web/onion-mkdocs/).

[Tor]: https://torproject.org
[MkDocs]: https://www.mkdocs.org/
[Material theme]: https://squidfunk.github.io/mkdocs-material/
