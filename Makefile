#
# Onion MkDocs Makefile.
#
# It can be used as an example for your documentation.
#

# Folder relative to this Makefile pointing to where the Onion MkDocs is
# installed.
#
# Customize to your needs in your main Makefile. Examples:
#
# ONION_MKDOCS_PATH = vendor/onion-mkdocs
# ONION_MKDOCS_PATH = vendors/onion-mkdocs
ONION_MKDOCS_PATH ?= .

# This is useful when developing your documentation locally and Onion MkDocs is
# not yet installed on your project but you don't want it to be a Git submodule.
#
# If you use this approach, make sure to at the Onion MkDocs path into your
# .gitignore.
vendoring:
	@test   -e $(ONION_MKDOCS_PATH) && git -C $(ONION_MKDOCS_PATH) pull || true
	@test ! -e $(ONION_MKDOCS_PATH) && git clone https://gitlab.torproject.org/tpo/web/onion-mkdocs.git $(ONION_MKDOCS_PATH) || true

# Include the Onion MkDocs Makefile
# See https://www.gnu.org/software/make/manual/html_node/Include.html
-include $(ONION_MKDOCS_PATH)/Makefile.onion-mkdocs
