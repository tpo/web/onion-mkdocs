#
# Onion MkDocs kvmx file - https://0xacab.org/rhatto/kvmx
#

# Hostname
hostname="onion-mkdocs"

# Which base box you should use. Leave unconfigured to use kvmx-create instead.
#basebox="bookworm"
basebox="dev"

# Set this is you want to be able to share multiple folders between host and guest using 9p.
# Needs ssh_support set to "y" and a workable SSH connection to the guest.
# Format: <id1>:<host-folder1>:<guest-mountpoint1>,<id2>:<host-folder2>:<guest-mountpoint2>[,...]
shared_folders="shared:.:/srv/shared"

# Absolute path for a provision script located inside the guest.
# Needs ssh_support set to "y" and a workable SSH connection to the guest.
provision_command="/usr/local/share/kvmx/provision/debian/development"
provision_command="$provision_command && /srv/shared/scripts/onion-mkdocs-provision-host"
provision_command="$provision_command && /srv/shared/scripts/onion-mkdocs-provision-build"
provision_command="$provision_command && /srv/shared/scripts/onion-mkdocs-provision-serve"

# Startup command
startup_command="hydractl aperiodic-upgrade"
# Make serve might running from the outside, so this is left commented
#startup_command="make -C /srv/shared serve"

# Set additional hostfwd mappings
port_mapping="hostfwd=tcp:127.0.0.1:8031-:8000"

# Use basebox image as a backing file for overlay images
# See https://wiki.archlinux.org/index.php/QEMU#Overlay_storage_images
backing_file="1"

# Memory
memory="512"

# Enables remote administration using SSH. With this configuration enabled,
# kvmx will be able to administer a running virtual machine using SSH access
# inside the virtual machine.
ssh_support="y"
